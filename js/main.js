$( document ).ready(function() {
    
  $('.rewiews-wrapper-inner').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows:false,
  dots:true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
    
    
     $('.big-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.little-slider',
             speed: 1000,
    infinite: true,
         adaptiveHeight: true,
    touchThreshold: 100,
         responsive: [
             {
                 breakpoint: 601,
                 adaptiveHeight: true
             }
         ]
      });
          $('.little-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.big-slider',
        dots: false,
        arrows: false,
        centerMode: false,
        focusOnSelect: true,
        vertical: false,
        responsive: [
          {
            breakpoint: 1025,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              vertical: false,
                centerMode:false
            }
          },
          {
            breakpoint: 769,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              vertical: false,
                centerMode:false
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              vertical: false
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              vertical: false
            }
          }
        ]
      });    
    
$('.more').click(function(){
    $(this).next().slideToggle();
     var x = $(this);
  if (x.text() === "Показать весь текст") {
    x.text('Свернуть');
  } else {
    x.text('Показать весь текст');
  }
    
});
    
$('.minus').click(function () {
			var $input = $(this).parent().find('input');
			var count = parseInt($input.val()) - 1;
			count = count < 1 ? 1 : count;
			$input.val(count);
			$input.change();
			return false;
		});
		$('.plus').click(function () {
			var $input = $(this).parent().find('input');
			$input.val(parseInt($input.val()) + 1);
			$input.change();
			return false;
		});
})

    
    
    
    
    
    
    
